/*
 * Copyright (C) 2022-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.jsonify;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayDeque;
import java.util.BitSet;
import java.util.Deque;
import java.util.Map;

/**
 * JSON reader. The input reader should ideally be buffered.
 * <p>
 * According to RFC8259, "The names within an object SHOULD be unique." This implementation does not check for duplicate
 * names and will overwrite earlier values with the last-read one when using {@link JsonReader#parseStructure()}, as
 * {@link JsonObject} is a {@link Map}. To preserve duplicate names and values, use {@link JsonReader#nextToken()}
 * exclusively.
 */
public class JsonReader implements JsonInput {
	/**
	 * The input reader.
	 */
	private final @NotNull Reader reader;

	/**
	 * Creates a json reader.
	 */
	public JsonReader(@NotNull Reader reader) {
		this.reader = reader;
	}

	//========================================

	/**
	 * Read input {@link Reader} to {@link JsonElement}.
	 *
	 * @param input The provided {@link Reader}.
	 *
	 * @throws IOException If an error occurred while reading input {@link Reader} or parsing input to
	 *        {@link JsonElement}.
	 */
	public static @NotNull JsonElement read(@NotNull Reader input) throws IOException {
		try (final JsonReader reader = new JsonReader(input)) {
			switch (reader.nextToken()) {
				case JsonElement element -> {
					// checking for EOF
					reader.nextToken();
					return element;
				}
				case JsonTokens.ARRAY_BEGIN, JsonTokens.OBJECT_BEGIN -> {
					final JsonElement structure = reader.parseStructure();
					// checking for EOF
					reader.nextToken();
					return structure;
				}
				default -> throw new AssertionError();
			}
		}
	}

	/**
	 * Read input {@link String} to {@link JsonElement}.
	 *
	 * @param input The provided {@link String}.
	 *
	 * @throws IllegalArgumentException If an error occurred while parsing input {@link String} to
	 *        {@link JsonElement}.
	 */
	public static @NotNull JsonElement read(@NotNull String input) {
		try (final StringReader reader = new StringReader(input)) {
			return read(reader);
		} catch (final IOException exception) {
			throw new IllegalArgumentException(exception);
		}
	}

	//========================================

	/**
	 * The codepoint that has been undone reading.
	 */
	private int undo = -1;

	/**
	 * Undo reading a codepoint.
	 */
	private void undo(int undo) {
		// assert undo < 0;
		this.undo = undo;
	}

	/**
	 * Get next codepoint.
	 */
	private int read() throws IOException {
		final int undo = this.undo;
		if (undo < 0) {
			int u0 = reader.read();
			if (u0 < 0 || !Character.isHighSurrogate((char) u0)) {
				// eof or normal character
				return u0;
			}
			int u1 = reader.read();
			if (u1 >= 0 && Character.isLowSurrogate((char) u1)) {
				// extended character
				return Character.toCodePoint((char) u0, (char) u1);
			}
			// invalid/incomplete pair
			throw new IOException("Invalid input surrogate pair.");
		} else {
			this.undo = -1;
			return undo;
		}

	}

	/**
	 * Get next character, skip all allowed whitespaces.
	 */
	private int readNonWhitespace() throws IOException {
		while (true) {
			final int character = read();
			switch (character) {
				case '\t', '\n', '\r', ' ' -> {
				}
				default -> {
					return character;
				}
			}
		}
	}

	//========================================

	/**
	 * Last states of the reader's state machine. This bit set will be used as a stack of state. A bit with value 0
	 * indicates the reader is currently inside an object. A bit with value 1 indicates the reader is currently inside
	 * an array.
	 */
	private final @NotNull BitSet lastStructures = new BitSet();

	/**
	 * Last index of last states array. An index smaller than 0 indicates the reader is at the top level, any other
	 * value indicate the reader is inside an object or an array.
	 */
	private int lastStructureIndex = -1;

	/**
	 * This state indicates that the reader is in error state, nothing else can be done except throwing.
	 */
	private static final int STATE_ERROR = -2;

	/**
	 * This state indicates that the reader is closed.
	 */
	private static final int STATE_CLOSED = -1;

	/**
	 * This state indicates that the reader expects the next token is a Name.
	 */
	private static final int STATE_IN_OBJECT_EXPECT_NAME = 0;

	/**
	 * This state indicates that the reader expects the next token is a Name or an ObjectEnd.
	 */
	private static final int STATE_IN_OBJECT_EXPECT_NAME_OR_OBJECT_END = 1;

	/**
	 * This state indicates that the reader expects the next token is a Value.
	 */
	private static final int STATE_EXPECT_VALUE = 2;

	/**
	 * This state indicates that the reader expects the next token is a Value or an ArrayEnd.
	 */
	private static final int STATE_IN_ARRAY_EXPECT_VALUE_OR_ARRAY_END = 3;

	/**
	 * Indicates that the reader has passed the {@link JsonTokens#STRUCTURE_END} token and is now awaiting a call to
	 * {@link #endStructure()}.
	 */
	private static final int STATE_STRUCTURE_END = 4;

	/**
	 * This state indicates that the reader expects the next token is an EOF.
	 */
	private static final int STATE_EXPECT_DOCUMENT_END = 5;

	/**
	 * Current state of the reader's state machine.
	 */
	private int state = STATE_EXPECT_VALUE;

	//========================================

	/**
	 * Parses and returns the next token.
	 */
	private @NotNull JsonToken nextTokenUnsafe() throws IOException {
		switch (state) {
			case STATE_IN_OBJECT_EXPECT_NAME, STATE_IN_OBJECT_EXPECT_NAME_OR_OBJECT_END -> {
				// parse a name
				final int c = readNonWhitespace();
				if (c == '}') {
					if (state == STATE_IN_OBJECT_EXPECT_NAME_OR_OBJECT_END) {
						this.state = STATE_STRUCTURE_END;
						return JsonTokens.STRUCTURE_END;
					}
				} else if (c == '\"') {
					final String string = stringOrName();
					if (readNonWhitespace() == ':') {
						this.state = STATE_EXPECT_VALUE;
						return new JsonName(string);
					}
				}
				throw new IOException("Unexpected character!");
			}
			case STATE_EXPECT_VALUE, STATE_IN_ARRAY_EXPECT_VALUE_OR_ARRAY_END -> {
				final int c = readNonWhitespace();
				if (c == '\"') {
					final String string = stringOrName();
					consumeSeparator();
					return new JsonString(string);
				} else if (c >= '0' && c <= '9' || c == '-') {
					final JsonNumber number = number(c);
					consumeSeparator();
					return number;
				} else if (c == 't') {
					if (read() == 'r' && read() == 'u' && read() == 'e') {
						consumeSeparator();
						return JsonKeyword.TRUE;
					}
				} else if (c == 'f') {
					if (read() == 'a' && read() == 'l' && read() == 's' && read() == 'e') {
						consumeSeparator();
						return JsonKeyword.FALSE;
					}
				} else if (c == 'n') {
					if (read() == 'u' && read() == 'l' && read() == 'l') {
						consumeSeparator();
						return JsonKeyword.NULL;
					}
				} else if (c == '[') {
					// push Array to the structure stack
					// set next expected token to be a Value
					this.state = STATE_IN_ARRAY_EXPECT_VALUE_OR_ARRAY_END;
					lastStructures.set(++this.lastStructureIndex);
					return JsonTokens.ARRAY_BEGIN;
				} else if (c == ']') {
					// closing an empty Array, or catching a stray array closing character
					if (state == STATE_IN_ARRAY_EXPECT_VALUE_OR_ARRAY_END && lastStructureIndex >= 0) {
						this.state = STATE_STRUCTURE_END;
						return JsonTokens.STRUCTURE_END;
					}
				} else if (c == '{') {
					// push Object to the structure stack
					// set next expected token to be a Name
					this.state = STATE_IN_OBJECT_EXPECT_NAME_OR_OBJECT_END;
					lastStructures.clear(++this.lastStructureIndex);
					return JsonTokens.OBJECT_BEGIN;
				}
				throw new IOException("Unexpected character!");
			}
			case STATE_STRUCTURE_END -> {
				return JsonTokens.STRUCTURE_END;
			}
			case STATE_EXPECT_DOCUMENT_END -> {
				final int c = readNonWhitespace();
				if (c < 0) return JsonTokens.EOF;
				throw new IOException("Unexpected character!");
			}
			default -> throw new AssertionError();
		}
	}

	/**
	 * Skips all tokens until skipping over the end of current structure.
	 */
	private void endStructureUnsafe() throws IOException {
		if (state == STATE_STRUCTURE_END) {
			// at the end of a structure
			// pop structure stack
			this.lastStructureIndex -= 1;
			consumeSeparator();
		} else if (lastStructureIndex >= 0) {
			// in the middle of a structure, so skip everything until the end
			final int currentStructureIndex = this.lastStructureIndex;
			while (currentStructureIndex <= lastStructureIndex) {
				//noinspection StatementWithEmptyBody
				while (nextTokenUnsafe() != JsonTokens.STRUCTURE_END) {
				}
				// pop structure stack
				this.lastStructureIndex -= 1;
				consumeSeparator();
			}
		} else {
			throw new IllegalStateException("Not in a structure!");
		}
	}

	/**
	 * Consume structure separator after a value and set state accordingly. This includes comma, array close bracket and
	 * object close bracket.
	 */
	private void consumeSeparator() throws IOException {
		if (lastStructureIndex >= 0) {
			// the reader is inside an object or an array
			final int c = readNonWhitespace();
			switch (c) {
				case ',' -> // not end of object/array yet
						this.state = lastStructures.get(lastStructureIndex)
								? STATE_EXPECT_VALUE // array
								: STATE_IN_OBJECT_EXPECT_NAME; // object
				case ']', '}' -> {
					// end of structure. Checking the closing character
					if (c == ']' ^ lastStructures.get(lastStructureIndex)) {
						throw new IOException("Unexpected character!");
					}
					this.state = STATE_STRUCTURE_END;
				}
				default -> throw new IOException("Unexpected character!");
			}
		} else {
			// the reader is at the top level, expect an EOF
			this.state = STATE_EXPECT_DOCUMENT_END;
		}
	}

	/**
	 * Consume a Number token and return the corresponding {@link JsonNumber}.
	 */
	private @NotNull JsonNumber number(int startCp) throws IOException {
		final StringBuilder builder = new StringBuilder();
		boolean integer = true;
		// first part: the integer
		int c = startCp;
		if (c == '-') {
			// minus sign
			builder.append('-');
			c = read();
		}
		if (c == '0') {
			// zero suffix
			builder.append('0');
			c = read();
		} else if (c >= '1' && c <= '9') {
			// digits
			do {
				builder.append((char) c);
				c = read();
			} while (c >= '0' && c <= '9');
		} else {
			throw new IOException("Invalid character in integer part of number!");
		}
		// second part: fraction
		if (c == '.') {
			integer = false;
			// set decimal
			builder.append('.');
			c = read();
			// at least one digit
			if (c >= '0' && c <= '9') {
				// digits
				do {
					builder.append((char) c);
					c = read();
				} while (c >= '0' && c <= '9');
			} else {
				throw new IOException("Invalid character in fraction part of number!");
			}
		}
		// third part: exponent
		if (c == 'e' || c == 'E') {
			integer = false;
			// set decimal
			builder.append((char) c);
			c = read();
			// sign
			if (c == '+' || c == '-') {
				builder.append((char) c);
				c = read();
			}
			// at least one digit
			if (c >= '0' && c <= '9') {
				// digits
				do {
					builder.append((char) c);
					c = read();
				} while (c >= '0' && c <= '9');
			} else {
				throw new IOException("Invalid character in exponent part of number!");
			}
		}
		undo(c);
		final String number = builder.toString();
		return integer
				? new JsonNumber(new BigInteger(number))
				: new JsonNumber(new BigDecimal(number));
	}

	/**
	 * Consume a String token and return the {@link String} value.
	 */
	private @NotNull String stringOrName() throws IOException {
		// the open quote are already consumed
		final StringBuilder builder = new StringBuilder();
		while (true) {
			final int c = read();
			if (c >= ' ' && c != '\\' && c != '"') {
				// the specification do have an upper limit for codepoint in string
				// currently it is the same as the upper limit of the Unicode table
				// here I deliberately skip the specification codepoint limit for future-proof
				builder.appendCodePoint(c);
			} else if (c == '\\') {
				final int d = read();
				switch (d) {
					case '"', '\\', '/' -> builder.append((char) d);
					case 't' -> builder.append('\t');
					case 'b' -> builder.append('\b');
					case 'n' -> builder.append('\n');
					case 'r' -> builder.append('\r');
					case 'f' -> builder.append('\f');
					case 'u' -> {
						int result = 1;
						do {
							int e = read();
							if (e >= '0' && e <= '9') {
								result = (result << 4) + e - '0';
							} else if (e >= 'A' && e <= 'F') {
								result = (result << 4) + e - 'A' + 10;
							} else if (e >= 'a' && e <= 'f') {
								result = (result << 4) + e - 'a' + 10;
							} else {
								throw new IOException("Invalid escape sequence in string!");
							}
						} while (result < 0x10000);
						builder.appendCodePoint(result - 0x10000);
					}
					default -> throw new IOException("Invalid escape sequence in string!");
				}
			} else if (c == '"') {
				return builder.toString();
			} else {
				throw new IOException("Invalid character in string!");
			}
		}
	}

	//========================================

	/**
	 * Parses an array or an object and all inner element inside it, without recursion.
	 */
	private @NotNull JsonElement parseStructureUnsafe() throws IOException {
		final Deque<JsonElement> stack = new ArrayDeque<>();
		stack.add(switch (state) {
			case STATE_IN_ARRAY_EXPECT_VALUE_OR_ARRAY_END -> new JsonArray();
			case STATE_IN_OBJECT_EXPECT_NAME_OR_OBJECT_END -> new JsonObject();
			default -> throw new IllegalStateException("Not in structure!");
		});
		while (true) {
			final JsonElement newElement = switch (stack.peek()) {
				case JsonArray array -> parseArrayUnsafe(array);
				case JsonObject object -> parseObjectUnsafe(object);
				case null, default -> throw new AssertionError();
			};
			if (newElement != null) {
				stack.push(newElement);
			} else {
				final JsonElement element = stack.pop();
				if (stack.isEmpty()) return element;
			}
		}
	}

	/**
	 * Parses an array, return the inner array or inner object if any, return null if end of current array.
	 */
	private @Nullable JsonElement parseArrayUnsafe(@NotNull JsonArray array) throws IOException {
		while (true) {
			final JsonToken token = nextTokenUnsafe();
			if (token instanceof JsonElement element) {
				array.add(element);
			} else if (token.equals(JsonTokens.STRUCTURE_END)) {
				endStructureUnsafe();
				return null;
			} else {
				final JsonElement element = switch (token) {
					case JsonTokens.ARRAY_BEGIN -> new JsonArray();
					case JsonTokens.OBJECT_BEGIN -> new JsonObject();
					default -> throw new AssertionError();
				};
				array.add(element);
				return element;
			}
		}
	}

	/**
	 * Parses an object, return the inner array or inner object if any, return null if end of current object.
	 */
	private @Nullable JsonElement parseObjectUnsafe(@NotNull JsonObject object) throws IOException {
		while (true) {
			switch (nextTokenUnsafe()) {
				case JsonName(String name) -> {
					final JsonToken token = nextTokenUnsafe();
					if (token instanceof JsonElement element) {
						object.put(name, element);
					} else {
						final JsonElement element = switch (token) {
							case JsonTokens.ARRAY_BEGIN -> new JsonArray();
							case JsonTokens.OBJECT_BEGIN -> new JsonObject();
							default -> throw new AssertionError();
						};
						object.put(name, element);
						return element;
					}
				}
				case JsonTokens.STRUCTURE_END -> {
					endStructureUnsafe();
					return null;
				}
				default -> throw new AssertionError();
			}
		}
	}

	//========================================

	/**
	 * Checks to make sure that the stream has not been closed and the reader is not in error state.
	 */
	private void ensureOpenAndValid() throws IOException {
		if (state == STATE_CLOSED) throw new IOException("Already closed!");
		if (state == STATE_ERROR) throw new IOException("Already failed!");
	}

	/**
	 * Close the JSON reader, also close the underlying reader.
	 */
	@Override
	public void close() throws IOException {
		if (state != STATE_CLOSED) {
			reader.close();
			lastStructures.clear();
			this.state = STATE_CLOSED;
		}
	}

	@Override
	public @NotNull JsonToken nextToken() throws IOException {
		ensureOpenAndValid();
		try {
			return nextTokenUnsafe();
		} catch (final IOException exception) {
			this.state = STATE_ERROR;
			throw exception;
		}
	}

	@Override
	public void endStructure() throws IOException {
		ensureOpenAndValid();
		try {
			endStructureUnsafe();
		} catch (final IOException exception) {
			this.state = STATE_ERROR;
			throw exception;
		}
	}

	@Override
	public @NotNull JsonElement parseStructure() throws IOException {
		ensureOpenAndValid();
		try {
			return parseStructureUnsafe();
		} catch (final IOException exception) {
			this.state = STATE_ERROR;
			throw exception;
		}
	}
}

