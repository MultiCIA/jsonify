/*
 * Copyright (C) 2022-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.jsonify;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayDeque;
import java.util.BitSet;
import java.util.Deque;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * JSON writer.
 */
public class JsonWriter implements JsonOutput {
	/**
	 * The output writer.
	 */
	private final @NotNull Writer writer;

	/**
	 * Creates a {@link JsonWriter}.
	 *
	 * @param writer The output {@link Writer} that this {@link JsonWriter} writes to.
	 */
	public JsonWriter(@NotNull Writer writer) {
		this.writer = writer;
	}

	//========================================

	/**
	 * Write the input {@link JsonElement} to output {@link Writer}.
	 *
	 * @throws IOException Throws if there is any error while writing the {@link JsonElement} to the {@link Writer}.
	 */
	public static void write(@NotNull Writer outputWriter, @NotNull JsonElement element) throws IOException {
		try (final JsonWriter writer = new JsonWriter(outputWriter)) {
			writer.value(element);
		}
	}

	/**
	 * Write the input {@link JsonElement} to output {@link String}.
	 *
	 * @param element The input {@link JsonElement}.
	 *
	 * @return The output JSON string corresponded to the input {@code element}.
	 */
	public static @NotNull String write(@NotNull JsonElement element) {
		try (final StringWriter writer = new StringWriter()) {
			JsonWriter.write(writer, element);
			return writer.toString();
		} catch (final IOException exception) {
			throw new RuntimeException(exception); // this should never happen
		}
	}

	//========================================

	/**
	 * Close the JSON writer, also close the underlying writer.
	 */
	@Override
	public void close() throws IOException {
		if (state != STATE_CLOSED_UNEXPECTEDLY && state != STATE_CLOSED_GRACEFULLY) {
			this.state = state == STATE_EXPECT_DOCUMENT_END ? STATE_CLOSED_GRACEFULLY : STATE_CLOSED_UNEXPECTEDLY;
			lastStructures.clear();
			writer.close();
		}
	}

	/**
	 * Check if the JSON writer is done writing the JSON. The writer is considered done writing the JSON when there is
	 * nothing more can be written after that point that makes the output JSON still valid.
	 */
	public boolean isDone() {
		return state == STATE_EXPECT_DOCUMENT_END || state == STATE_CLOSED_GRACEFULLY;
	}

	/**
	 * Checks to make sure that the stream has not been closed and the writer is not in error state.
	 */
	private void ensureOpenAndValid() throws IOException {
		if (state == STATE_CLOSED_UNEXPECTEDLY || state == STATE_CLOSED_GRACEFULLY) {
			throw new IOException("Already closed!");
		}
		if (state == STATE_ERROR) throw new IOException("Already failed!");
	}

	@Override
	public void beginArray() throws IOException {
		ensureOpenAndValid();
		try {
			beginArrayUnsafe();
		} catch (final IOException exception) {
			// unexpected throw
			this.state = STATE_ERROR;
			throw exception;
		}
	}

	@Override
	public void beginObject() throws IOException {
		ensureOpenAndValid();
		try {
			beginObjectUnsafe();
		} catch (final IOException exception) {
			// unexpected throw
			this.state = STATE_ERROR;
			throw exception;
		}
	}

	@Override
	public void end() throws IOException {
		ensureOpenAndValid();
		try {
			endUnsafe();
		} catch (final IOException exception) {
			// unexpected throw
			this.state = STATE_ERROR;
			throw exception;
		}
	}

	@Override
	public void name(@NotNull String name) throws IOException {
		ensureOpenAndValid();
		try {
			nameUnsafe(name);
		} catch (final IOException exception) {
			// unexpected throw
			this.state = STATE_ERROR;
			throw exception;
		}
	}

	@Override
	public void value(@NotNull JsonElement element) throws IOException {
		ensureOpenAndValid();
		try {
			valueUnsafe(element);
		} catch (final IOException exception) {
			// unexpected throw
			this.state = STATE_ERROR;
			throw exception;
		}
	}

	//========================================

	/**
	 * Last states of the writer's state machine. This bit set will be used as a stack of state. A bit with value 0
	 * indicates the writer is currently inside an object. A bit with value 1 indicates the writer is currently inside
	 * an array.
	 */
	private final @NotNull BitSet lastStructures = new BitSet();

	/**
	 * Last index of last states array. An index smaller than 0 indicates the writer is at the top level, any other
	 * value indicate the writer is inside an object or an array.
	 */
	private int lastStructureIndex = -1;

	/**
	 * Indicates that an error occurred while writing, rendering the document invalid. No further actions can be taken
	 * other than throwing an exception.
	 */
	private static final int STATE_ERROR = -3;

	/**
	 * Indicates that the writer was unexpectedly closed, rendering the document invalid.
	 */
	private static final int STATE_CLOSED_UNEXPECTEDLY = -2;

	/**
	 * This state indicates that the current writer is closed gracefully, the document is valid.
	 */
	private static final int STATE_CLOSED_GRACEFULLY = -1;

	/**
	 * This state indicates that the writer is at the beginning of an object, and the writer expects the next token is a
	 * Name or an ObjectEnd.
	 */
	private static final int STATE_IN_OBJECT_EXPECT_NAME_NO_SEPARATOR = 0;

	/**
	 * This state indicates that the writer is in the middle of an object, and the writer expects the next token is a
	 * Name or an ObjectEnd, and will automatically write a Comma before writing the name if the next token is a Name.
	 */
	private static final int STATE_IN_OBJECT_EXPECT_NAME_AFTER_COMMA = 1;

	/**
	 * This state indicates that the writer is either at the beginning of an array. and the writer expects the next
	 * token is a Value.
	 */
	private static final int STATE_IN_ARRAY_EXPECT_VALUE_NO_SEPARATOR = 2;

	/**
	 * This state indicates that the writer is in the middle of an array, ann the writer expects the next token is a
	 * Value, and will automatically write a Comma before writing the value.
	 */
	private static final int STATE_IN_ARRAY_EXPECT_VALUE_AFTER_COMMA = 3;

	/**
	 * This state indicates that the writer is after the name in the middle of an object, ann the writer expects the
	 * next token is a Value, and will automatically write a Colon before writing the value.
	 */
	private static final int STATE_IN_OBJECT_EXPECT_VALUE_AFTER_COLON = 4;

	/**
	 * This state indicates that the writer expects nothing else will be written, and waiting for a call to close the
	 * JSON writer.
	 */
	private static final int STATE_EXPECT_DOCUMENT_END = 5;

	/**
	 * Current state of the writer's state machine.
	 */
	private int state = STATE_IN_ARRAY_EXPECT_VALUE_NO_SEPARATOR;

	//========================================

	private void beginArrayUnsafe() throws IOException {
		switch (state) {
			case STATE_IN_OBJECT_EXPECT_VALUE_AFTER_COLON -> writer.write(":[");
			case STATE_IN_ARRAY_EXPECT_VALUE_AFTER_COMMA -> writer.write(",[");
			case STATE_IN_ARRAY_EXPECT_VALUE_NO_SEPARATOR -> writer.write('[');
			default -> throw new IllegalStateException("Array begin unexpected!");
		}
		lastStructures.set(++this.lastStructureIndex);
		this.state = STATE_IN_ARRAY_EXPECT_VALUE_NO_SEPARATOR;
	}

	private void beginObjectUnsafe() throws IOException {
		switch (state) {
			case STATE_IN_OBJECT_EXPECT_VALUE_AFTER_COLON -> writer.write(":{");
			case STATE_IN_ARRAY_EXPECT_VALUE_AFTER_COMMA -> writer.write(",{");
			case STATE_IN_ARRAY_EXPECT_VALUE_NO_SEPARATOR -> writer.write('{');
			default -> throw new IllegalStateException("Object begin unexpected!");
		}
		lastStructures.clear(++this.lastStructureIndex);
		this.state = STATE_IN_OBJECT_EXPECT_NAME_NO_SEPARATOR;
	}

	private void endUnsafe() throws IOException {
		if (lastStructureIndex < 0) throw new IllegalStateException("Structure end unexpected!");
		// write expected structure end
		writer.write(switch (state) {
			case STATE_IN_OBJECT_EXPECT_NAME_NO_SEPARATOR, STATE_IN_OBJECT_EXPECT_NAME_AFTER_COMMA -> '}'; // object
			case STATE_IN_ARRAY_EXPECT_VALUE_NO_SEPARATOR, STATE_IN_ARRAY_EXPECT_VALUE_AFTER_COMMA -> ']'; // array
			default -> throw new IllegalStateException("Structure end unexpected!");
		});
		// remove that last object from the last structures stack
		this.lastStructureIndex -= 1;
		// check the last structures stack to determine the state
		this.state = lastStructureIndex >= 0
				? lastStructures.get(lastStructureIndex)
				? STATE_IN_ARRAY_EXPECT_VALUE_AFTER_COMMA
				: STATE_IN_OBJECT_EXPECT_NAME_AFTER_COMMA
				: STATE_EXPECT_DOCUMENT_END;
	}

	private void nameUnsafe(@NotNull String name) throws IOException {
		switch (state) {
			case STATE_IN_OBJECT_EXPECT_NAME_AFTER_COMMA, STATE_IN_OBJECT_EXPECT_NAME_NO_SEPARATOR -> {
				// write comma
				if (state == STATE_IN_OBJECT_EXPECT_NAME_AFTER_COMMA) writer.write(',');
				escapeAndWriteString(name);
				this.state = STATE_IN_OBJECT_EXPECT_VALUE_AFTER_COLON;
			}
			default -> throw new IllegalStateException("Name unexpected!");
		}
	}

	//========================================

	/**
	 * A container for the iterator of the {@link JsonArray}.
	 *
	 * @param array The {@link JsonArray}.
	 * @param iterator The iterator of a {@link JsonArray}.
	 */
	private record ArrayIterator(
			@NotNull JsonArray array,
			@NotNull Iterator<@NotNull JsonElement> iterator
	) {
	}

	/**
	 * A container for the iterator of the {@link JsonObject}.
	 *
	 * @param object The {@link JsonObject}
	 * @param iterator The iterator of the {@link JsonObject}.
	 */
	private record ObjectIterator(
			@NotNull JsonObject object,
			@NotNull Iterator<Map.@NotNull Entry<@NotNull String, @NotNull JsonElement>> iterator
	) {
	}

	/**
	 * Write a {@link JsonElement} value.
	 */
	private void valueUnsafe(@NotNull JsonElement element) throws IOException {
		final Map<JsonElement, Boolean> recursionSet = new IdentityHashMap<>();
		final Deque<Record> stack = new ArrayDeque<>();
		while (true) {
			// write element
			switch (element) {
				case JsonNumber number -> writeValueRawAndCheckState(number.value().toString());
				case JsonString value -> writeValueStringAndCheckState(value.value());
				case JsonKeyword value -> writeValueRawAndCheckState(switch (value) {
					case TRUE -> "true";
					case FALSE -> "false";
					case NULL -> "null";
				});
				case JsonArray array -> {
					// check recursion
					if (recursionSet.put(array, Boolean.TRUE) != null) {
						throw new IllegalArgumentException("Recursive structure detected!");
					}
					// write array
					beginArrayUnsafe();
					stack.push(new ArrayIterator(array, array.iterator()));
				}
				case JsonObject object -> {
					// check recursion
					if (recursionSet.put(object, Boolean.TRUE) != null) {
						throw new IllegalArgumentException("Recursive structure detected!");
					}
					// write object
					beginObjectUnsafe();
					stack.push(new ObjectIterator(object, object.entrySet().iterator()));
				}
			}
			element = null;
			do {
				// get next element
				switch (stack.peek()) {
					case ArrayIterator(JsonArray array, Iterator<JsonElement> iterator) -> {
						if (iterator.hasNext()) {
							// next element
							element = iterator.next();
						} else {
							// write array end
							endUnsafe();
							// remove from stack & recursion set
							recursionSet.remove(array);
							stack.pop();
							if (stack.isEmpty()) return;
						}
					}
					case ObjectIterator(JsonObject object, Iterator<Map.Entry<String, JsonElement>> iterator) -> {
						if (iterator.hasNext()) {
							// next name and element
							final Map.Entry<String, JsonElement> entry = iterator.next();
							nameUnsafe(entry.getKey());
							element = entry.getValue();
						} else {
							// write object end
							endUnsafe();
							// remove from stack & recursion set
							recursionSet.remove(object);
							stack.pop();
							if (stack.isEmpty()) return;
						}
					}
					case null -> {
						return;
					}
					default -> throw new AssertionError();
				}
			} while (element == null);
		}
	}

	//========================================

	/**
	 * Write value in raw mode instead of string mode. Raw mode does not escape the string.
	 */
	private void writeValueRawAndCheckState(@NotNull String rawValue) throws IOException {
		writeValueSeparatorAndCheckState();
		writer.write(rawValue);
	}

	/**
	 * Write value in string mode instead of raw mode. String mode does escape the string and put them in quote.
	 */
	private void writeValueStringAndCheckState(@NotNull String string) throws IOException {
		writeValueSeparatorAndCheckState();
		escapeAndWriteString(string);
	}

	/**
	 * Write the separator (COLON for the name value separator, COMMA for the value separator or for name-value pair
	 * separator)
	 */
	private void writeValueSeparatorAndCheckState() throws IOException {
		switch (state) {
			case STATE_IN_OBJECT_EXPECT_VALUE_AFTER_COLON -> {
				this.state = STATE_IN_OBJECT_EXPECT_NAME_AFTER_COMMA;
				writer.write(':');
			}
			case STATE_IN_ARRAY_EXPECT_VALUE_AFTER_COMMA -> writer.write(',');
			case STATE_IN_ARRAY_EXPECT_VALUE_NO_SEPARATOR -> this.state = lastStructureIndex >= 0
					? STATE_IN_ARRAY_EXPECT_VALUE_AFTER_COMMA
					: STATE_EXPECT_DOCUMENT_END;
			default -> throw new IllegalStateException("Value unexpected!");
		}
	}

	/**
	 * Escape string and write out the escaped string.
	 */
	private void escapeAndWriteString(@NotNull String string) throws IOException {
		writer.write('"');
		final int length = string.length();
		int count = 0;
		for (int index = 0; index < length; index++) {
			final int c = string.charAt(index);
			if (c >= ' ' && c != '"' && c != '\\') {
				count += 1;
			} else {
				if (count > 0) {
					writer.write(string, index - count, count);
					count = 0;
				}
				switch (c) {
					case '"' -> writer.write("\\\"");
					case '\\' -> writer.write("\\\\");
					case '\r' -> writer.write("\\r");
					case '\n' -> writer.write("\\n");
					case '\t' -> writer.write("\\t");
					case '\b' -> writer.write("\\b");
					case '\f' -> writer.write("\\f");
					default -> {
						writer.write("\\u00");
						writer.write(c >= 0x10 ? '1' : '0');
						writer.write(c + (c >= 10 ? 'A' - 10 : '0'));
					}
				}
			}
		}
		if (count > 0) writer.write(string, length - count, count);
		writer.write('"');
	}
}
