/*
 * Copyright (C) 2022-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.jsonify;

import org.jetbrains.annotations.NotNull;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * A JSON number.
 * <p>
 * This implementation will reserve the exact value that appeared in the JSON: big integers are returned as
 * {@link BigInteger}, and high precision decimals are returned as {@link BigDecimal}. This does not violate the JSON
 * specification, as by RFC7159, the JSON specification "allows implementations to set limits on the range and precision
 * of numbers accepted.". However, the specification also suggested that "good interoperability can be achieved by
 * implementations that expect no more precision or range than these (IEEE 754-2008 binary64, usually called double
 * precision) provide, in the sense that implementations will approximate JSON numbers within the expected precision.".
 * In other words, don't expect other libraries to respect the exact values of higher-than-double-precision decimals
 * that this library produce.
 * <p>
 * Also, please note that JSON specification doesn't allow NaN and Infinity.
 */
public record JsonNumber(@NotNull Number value) implements JsonImmutable, JsonToken {
	/**
	 * Create a {@link JsonNumber} with a {@code long} value.
	 */
	public JsonNumber(long value) {
		this(Long.valueOf(value));
	}

	/**
	 * Create a {@link JsonNumber} with a {@code double} value.
	 *
	 * @throws IllegalArgumentException If the input value is NaN or Infinity.
	 */
	public JsonNumber(double value) {
		this(tryDoubleToLong(value));
	}

	/**
	 * Creates a {@link JsonNumber} from a {@link Number}, supporting {@link Byte}, {@link Short}, {@link Integer},
	 * {@link Long}, {@link Float}, {@link Double}, {@link BigInteger}, and {@link BigDecimal}.
	 * <p>
	 * This constructor normalizes the number to maintain precision (e.g., converting {@link Double} to {@link Long},
	 * {@link BigDecimal} to {@link BigInteger}, or {@link BigInteger} to {@link Long} when appropriate), ensuring the
	 * JSON representation remains consistent between serialization and deserialization.
	 *
	 * @throws IllegalArgumentException If the input value is NaN, Infinity, or an unsupported type.
	 */
	public JsonNumber(@NotNull Number value) {
		this.value = switch (value) {
			case Byte byteValue -> byteValue.longValue();
			case Short shortValue -> shortValue.longValue();
			case Integer integerValue -> integerValue.longValue();
			case Long longValue -> longValue;
			case Float floatValue -> tryDoubleToLong(floatValue.doubleValue());
			case Double doubleValue -> tryDoubleToLong(doubleValue);
			case BigInteger bigInteger -> tryBigIntegerToLong(bigInteger);
			case BigDecimal bigDecimal -> tryBigDecimalToDouble(bigDecimal);
			default -> throw new IllegalArgumentException("Unsupported number type!");
		};
	}


	// try to convert Double to Long without losing precision
	private static @NotNull Number tryDoubleToLong(@NotNull Double doubleValue) {
		if (!Double.isFinite(doubleValue)) throw new IllegalArgumentException("Invalid number value!");
		final long longValue = doubleValue.longValue();
		return Double.compare(longValue, doubleValue) != 0 ? doubleValue : longValue;
	}

	// try to convert BigInteger to Long without losing precision
	private static @NotNull Number tryBigIntegerToLong(@NotNull BigInteger integer) {
		return integer.bitLength() <= 63 ? integer.longValue() : integer;
	}

	// try to convert BigDecimal to Double without losing precision
	private static @NotNull Number tryBigDecimalToDouble(@NotNull BigDecimal decimal) {
		// double is precision(15..17) scale[-1022;1023]
		final int scale = decimal.scale();
		if (scale < -1022 || scale > 1023) return decimal;
		if (scale == 0) return tryBigIntegerToLong(decimal.toBigIntegerExact());
		final int precision = decimal.precision();
		if (precision > 17) return decimal;
		final double doubleValue = decimal.doubleValue(); // this is always finite
		final String formatString = "%." + precision + 'e';
		final String doubleString = String.format(formatString, doubleValue);
		final String decimalString = String.format(formatString, decimal);
		return doubleString.equals(decimalString) ? doubleValue : decimal;
	}


	/**
	 * Check if the inner value is an integer (a {@link Long} or a {@link BigInteger}).
	 */
	public boolean isInteger() {
		return value instanceof Long || value instanceof BigInteger;
	}

	/**
	 * Check if the inner value is a decimal (a {@link Double} or a {@link BigDecimal}).
	 */
	public boolean isDecimal() {
		return value instanceof Double || value instanceof BigDecimal;
	}

	/**
	 * Check if the inner value is a big number (a {@link BigInteger} or a {@link BigDecimal}).
	 */
	public boolean isBig() {
		return value instanceof BigInteger || value instanceof BigDecimal;
	}


	/**
	 * Down-casts this to {@link JsonNumber} or throws {@link ClassCastException} if failed.
	 *
	 * @return the {@link JsonNumber}.
	 */
	@Override
	public @NotNull JsonNumber asJsonNumber() {
		return this;
	}

	/**
	 * Converts the inner value to a {@code long} without preserving precision.
	 *
	 * @return The {@code long} representation of the value.
	 */
	public long toLong() {
		return value.longValue();
	}

	/**
	 * Converts the inner value to a {@code double} without preserving precision.
	 *
	 * @return The {@code double} representation of the value.
	 */
	public double toDouble() {
		return value.doubleValue();
	}

	/**
	 * Converts the inner value to a {@link BigInteger} without preserving precision.
	 *
	 * @return The {@link BigInteger} representation of the value.
	 */
	public @NotNull BigInteger toBigInteger() {
		return switch (value) {
			case Long longValue -> BigInteger.valueOf(longValue);
			case Double doubleValue -> BigDecimal.valueOf(doubleValue).toBigInteger();
			case BigInteger bigInteger -> bigInteger;
			case BigDecimal bigDecimal -> bigDecimal.toBigInteger();
			default -> throw new AssertionError(); // this should never happen
		};
	}

	/**
	 * Converts the inner value to a {@link BigDecimal} without preserving precision.
	 *
	 * @return The {@link BigDecimal} representation of the value.
	 */
	public @NotNull BigDecimal toBigDecimal() {
		return switch (value) {
			case Long longValue -> BigDecimal.valueOf(longValue);
			case Double doubleValue -> BigDecimal.valueOf(doubleValue);
			case BigInteger bigInteger -> new BigDecimal(bigInteger);
			case BigDecimal bigDecimal -> bigDecimal;
			default -> throw new AssertionError(); // this should never happen
		};
	}
}
