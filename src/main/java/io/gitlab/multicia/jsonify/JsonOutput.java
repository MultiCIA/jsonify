/*
 * Copyright (C) 2022-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.jsonify;

import org.jetbrains.annotations.NotNull;

import java.io.Closeable;
import java.io.IOException;

public interface JsonOutput extends Closeable {
	/**
	 * Begins an array.
	 *
	 * @throws IllegalStateException if beginning an array is unexpected.
	 * @throws IOException if an error occurs during writing.
	 */
	void beginArray() throws IOException;

	/**
	 * Begins an object.
	 *
	 * @throws IllegalStateException if beginning an object is unexpected.
	 * @throws IOException if an error occurs during writing.
	 */
	void beginObject() throws IOException;

	/**
	 * End an array or an object.
	 *
	 * @throws IllegalStateException if ending a structure is unexpected.
	 * @throws IOException if an error occurs during writing.
	 */
	void end() throws IOException;

	/**
	 * Writes a name.
	 *
	 * @throws IllegalStateException if writing a name is unexpected.
	 * @throws IOException if an error occurs during writing.
	 */
	void name(@NotNull String name) throws IOException;

	/**
	 * Write a {@link JsonElement} value.
	 *
	 * @throws IllegalStateException if writing a value is unexpected, or if recursion is detected.
	 * @throws IOException if an error occurs during writing.
	 */
	void value(@NotNull JsonElement element) throws IOException;
}
