/*
 * Copyright (C) 2022-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.jsonify;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public record JsonString(@NotNull String value) implements JsonImmutable, JsonToken {
	/**
	 * Create a {@link JsonString} with a {@code char} value.
	 */
	public JsonString(char value) {
		this(String.valueOf(value));
	}

	/**
	 * Create a {@link JsonString} from a {@link CharSequence} value.
	 */
	public JsonString(@NotNull CharSequence value) {
		this(value.toString());
	}


	/**
	 * Down-casts this to {@link JsonString} or throws {@link ClassCastException} if failed.
	 *
	 * @return the {@link JsonString}.
	 */
	@Override
	public @NotNull JsonString asJsonString() {
		return this;
	}

	/**
	 * Converts the inner value to a {@link Character} by taking the first {@code char} if any.
	 *
	 * @return The {@link Character} value or {@code null} if the inner value is empty.
	 */
	public @Nullable Character toChar() {
		return value.isEmpty() ? null : value.charAt(0);
	}
}
