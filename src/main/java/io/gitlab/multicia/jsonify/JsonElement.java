/*
 * Copyright (C) 2022-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.jsonify;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.IdentityHashMap;
import java.util.Map;

public sealed interface JsonElement permits JsonArray, JsonImmutable, JsonObject {
	/**
	 * Convert a {@link Boolean} to {@link JsonElement}.
	 *
	 * @param value the input {@link Boolean} to convert.
	 *
	 * @return the corresponding {@link JsonKeyword}.
	 */
	static @NotNull JsonKeyword of(@Nullable Boolean value) {
		return value == null ? JsonKeyword.NULL : value ? JsonKeyword.TRUE : JsonKeyword.FALSE;
	}

	/**
	 * Convert a {@link Character} to {@link JsonElement}.
	 *
	 * @param value the input {@link Character} to convert.
	 *
	 * @return the corresponding {@link JsonString} or {@link JsonKeyword#NULL} if the provided {@code value} is
	 *        {@code null}.
	 */
	static @NotNull JsonElement of(@Nullable Character value) {
		return value != null ? new JsonString(value) : JsonKeyword.NULL;
	}

	/**
	 * Convert a {@link CharSequence} to {@link JsonElement}.
	 *
	 * @param value the input {@link CharSequence} to convert.
	 *
	 * @return the corresponding {@link JsonString} or {@link JsonKeyword#NULL} if the provided {@code value} is
	 *        {@code null}.
	 */
	static @NotNull JsonElement of(@Nullable CharSequence value) {
		return value != null ? new JsonString(value) : JsonKeyword.NULL;
	}

	/**
	 * Convert a {@link Number} to {@link JsonElement} or throws {@link IllegalArgumentException} if failed.
	 *
	 * @param value the input {@link Number} to convert.
	 *
	 * @return the corresponding {@link JsonNumber} or {@link JsonKeyword#NULL} if the provided {@code value} is
	 *        {@code null}.
	 *
	 * @throws IllegalArgumentException if the provided value is invalid or is an unsupported type.
	 */
	static @NotNull JsonElement of(@Nullable Number value) {
		return value != null ? new JsonNumber(value) : JsonKeyword.NULL;
	}

	/**
	 * Convert a generic {@link Object} to {@link JsonElement} or throws {@link IllegalArgumentException} if failed.
	 * <p>
	 * All supported {@link Number} types are {@link Byte}, {@link Short}, {@link Integer}, {@link Long}, {@link Float},
	 * {@link Double}, {@link BigInteger}, {@link BigDecimal}. Also supports {@link Boolean}, {@link Character},
	 * {@link CharSequence},{@link Map} of {@link CharSequence}s to supported {@link Object}s, and {@link Collection} of
	 * supported {@link Object}s.
	 *
	 * @param value the input {@link Object} to convert.
	 *
	 * @return the corresponding {@link JsonElement} or {@link JsonKeyword#NULL} if the provided {@code value} is
	 *        {@code null}.
	 *
	 * @throws IllegalArgumentException If there is a recursive structure, an invalid value or an unsupported value.
	 */
	static @NotNull JsonElement of(@Nullable Object value) {
		return convert(value, null);
	}

	// Convert a generic Object to JsonElement or throws IllegalArgumentException if failed.
	private static @NotNull JsonElement convert(@Nullable Object object,
			@Nullable Map<@NotNull Object, @NotNull Boolean> identityMap) {
		return switch (object) {
			case null -> JsonKeyword.NULL;
			case JsonElement element -> element;
			case Boolean value -> value ? JsonKeyword.TRUE : JsonKeyword.FALSE;
			case Character value -> new JsonString(value);
			case CharSequence value -> new JsonString(value);
			case Number value -> new JsonNumber(value);
			case Collection<?> collection -> {
				// check for recursive structure
				final Map<Object, Boolean> anyIdentityMap = identityMap != null ? identityMap : new IdentityHashMap<>();
				if (anyIdentityMap.put(collection, Boolean.TRUE) != null) {
					throw new IllegalArgumentException("Recursive structure detected!");
				}
				// convert collection to array
				final JsonArray value = new JsonArray();
				for (final Object innerObject : collection) {
					value.add(convert(innerObject, anyIdentityMap));
				}
				anyIdentityMap.remove(collection);
				yield value;
			}
			case Map<?, ?> map -> {
				// check for recursive structure
				final Map<Object, Boolean> anyIdentityMap = identityMap != null ? identityMap : new IdentityHashMap<>();
				if (anyIdentityMap.put(map, Boolean.FALSE) != null) {
					throw new IllegalArgumentException("Recursive structure detected!");
				}
				// convert map to object
				final JsonObject value = new JsonObject();
				for (final Map.Entry<?, ?> entry : map.entrySet()) {
					if (entry.getKey() instanceof CharSequence name) {
						value.put(name.toString(), convert(entry.getValue(), anyIdentityMap));
					} else {
						throw new IllegalArgumentException("Cannot convert map key to a String!");
					}
				}
				anyIdentityMap.remove(map);
				yield value;
			}
			default -> throw new IllegalArgumentException("Unexpected type of value!");
		};
	}

	// =================================

	/**
	 * Converts this to {@link Boolean} or throws {@link ClassCastException} if failed.
	 *
	 * @param element The input {@link JsonElement} to convert.
	 *
	 * @return the {@link Boolean} or {@code null}.
	 */
	static @Nullable Boolean toBoolean(@Nullable JsonElement element) {
		return element != null ? element.asJsonKeyword().toBoolean() : null;
	}

	/**
	 * Converts this to {@link String} or throws {@link ClassCastException} if failed.
	 *
	 * @param element The input {@link JsonElement} to convert.
	 *
	 * @return the {@link String} or {@code null}.
	 */
	static @Nullable String toString(@Nullable JsonElement element) {
		return element != null && element != JsonKeyword.NULL ? element.asJsonString().value() : null;
	}

	/**
	 * Converts this to {@link Character} or throws {@link ClassCastException} if failed.
	 *
	 * @param element The input {@link JsonElement} to convert.
	 *
	 * @return the {@link Character} or {@code null}.
	 */
	static @Nullable Character toCharacter(@Nullable JsonElement element) {
		return element != null && element != JsonKeyword.NULL ? element.asJsonString().toChar() : null;
	}

	/**
	 * Converts this to {@link Number} or throws {@link ClassCastException} if failed.
	 *
	 * @param element The input {@link JsonElement} to convert.
	 *
	 * @return the {@link Number} or {@code null}.
	 */
	static @Nullable Number toNumber(@Nullable JsonElement element) {
		return element != null && element != JsonKeyword.NULL ? element.asJsonNumber().value() : null;
	}

	/**
	 * Converts this to {@link Long} or throws {@link ClassCastException} if failed.
	 *
	 * @param element The input {@link JsonElement} to convert.
	 *
	 * @return the {@link Long} or {@code null}.
	 */
	static @Nullable Long toLong(@Nullable JsonElement element) {
		return element != null && element != JsonKeyword.NULL ? element.asJsonNumber().toLong() : null;
	}

	/**
	 * Converts this to {@link Double} or throws {@link ClassCastException} if failed.
	 *
	 * @param element The input {@link JsonElement} to convert.
	 *
	 * @return the {@link Double} or {@code null}.
	 */
	static @Nullable Double toDouble(@Nullable JsonElement element) {
		return element != null && element != JsonKeyword.NULL ? element.asJsonNumber().toDouble() : null;
	}

	/**
	 * Converts this to {@link BigInteger} or throws {@link ClassCastException} if failed.
	 *
	 * @param element The input {@link JsonElement} to convert.
	 *
	 * @return the {@link BigInteger} or {@code null}.
	 */
	static @Nullable BigInteger toBigInteger(@Nullable JsonElement element) {
		return element != null && element != JsonKeyword.NULL ? element.asJsonNumber().toBigInteger() : null;
	}

	/**
	 * Converts this to {@link BigDecimal} or throws {@link ClassCastException} if failed.
	 *
	 * @param element The input {@link JsonElement} to convert.
	 *
	 * @return the {@link BigDecimal} or {@code null}.
	 */
	static @Nullable BigDecimal toBigDecimal(@Nullable JsonElement element) {
		return element != null && element != JsonKeyword.NULL ? element.asJsonNumber().toBigDecimal() : null;
	}

	// =================================

	/**
	 * Casts this to {@link JsonImmutable} or throws {@link ClassCastException} if failed.
	 *
	 * @param element The input {@link JsonElement} to cast.
	 *
	 * @return the {@link JsonImmutable}, or {@code null} if the provided {@code element} is {@code null} or
	 *        {@link JsonKeyword#NULL}.
	 */
	static @Nullable JsonImmutable asJsonImmutable(@Nullable JsonElement element) {
		return element != null && element != JsonKeyword.NULL ? element.asJsonImmutable() : null;
	}

	/**
	 * Casts this to {@link JsonKeyword} or throws {@link ClassCastException} if failed.
	 *
	 * @param element The input {@link JsonElement} to cast.
	 *
	 * @return the {@link JsonKeyword}, or {@code null} if the provided {@code element} is {@code null} or
	 *        {@link JsonKeyword#NULL}.
	 */
	static @Nullable JsonKeyword asJsonKeyword(@Nullable JsonElement element) {
		return element != null && element != JsonKeyword.NULL ? element.asJsonKeyword() : null;
	}

	/**
	 * Casts this to {@link JsonString} or throws {@link ClassCastException} if failed.
	 *
	 * @param element The input {@link JsonElement} to cast.
	 *
	 * @return the {@link JsonString}, or {@code null} if the provided {@code element} is {@code null} or
	 *        {@link JsonKeyword#NULL}.
	 */
	static @Nullable JsonString asJsonString(@Nullable JsonElement element) {
		return element != null && element != JsonKeyword.NULL ? element.asJsonString() : null;
	}

	/**
	 * Casts this to {@link JsonNumber} or throws {@link ClassCastException} if failed.
	 *
	 * @param element The input {@link JsonElement} to cast.
	 *
	 * @return the {@link JsonNumber}, or {@code null} if the provided {@code element} is {@code null} or
	 *        {@link JsonKeyword#NULL}.
	 */
	static @Nullable JsonNumber asJsonNumber(@Nullable JsonElement element) {
		return element != null && element != JsonKeyword.NULL ? element.asJsonNumber() : null;
	}

	/**
	 * Casts this to {@link JsonArray} or throws {@link ClassCastException} if failed.
	 *
	 * @param element The input {@link JsonElement} to cast.
	 *
	 * @return the {@link JsonArray}, or {@code null} if the provided {@code element} is {@code null} or
	 *        {@link JsonKeyword#NULL}.
	 */
	static @Nullable JsonArray asJsonArray(@Nullable JsonElement element) {
		return element != null && element != JsonKeyword.NULL ? element.asJsonArray() : null;
	}

	/**
	 * Casts this to {@link JsonObject} or throws {@link ClassCastException} if failed.
	 *
	 * @param element The input {@link JsonElement} to cast.
	 *
	 * @return the {@link JsonObject}, or {@code null} if the provided {@code element} is {@code null} or
	 *        {@link JsonKeyword#NULL}.
	 */
	static @Nullable JsonObject asJsonObject(@Nullable JsonElement element) {
		return element != null && element != JsonKeyword.NULL ? element.asJsonObject() : null;
	}

	// =================================

	/**
	 * Casts this object to {@link JsonImmutable}.
	 *
	 * @return this object as a {@link JsonImmutable}.
	 *
	 * @throws ClassCastException If this object is not a {@link JsonImmutable}.
	 */
	default @NotNull JsonImmutable asJsonImmutable() {
		throw new ClassCastException("Not a JsonImmutable!");
	}

	/**
	 * Casts this object to {@link JsonKeyword}.
	 *
	 * @return this object as a {@link JsonKeyword}.
	 *
	 * @throws ClassCastException If this object is not a {@link JsonKeyword}.
	 */
	default @NotNull JsonKeyword asJsonKeyword() {
		throw new ClassCastException("Not a JsonKeyword!");
	}


	/**
	 * Casts this object to {@link JsonString}.
	 *
	 * @return this object as a {@link JsonString}.
	 *
	 * @throws ClassCastException If this object is not a {@link JsonString}.
	 */
	default @NotNull JsonString asJsonString() {
		throw new ClassCastException("Not a JsonString!");
	}

	/**
	 * Casts this object to {@link JsonNumber}.
	 *
	 * @return this object as a {@link JsonNumber}.
	 *
	 * @throws ClassCastException If this object is not a {@link JsonNumber}.
	 */
	default @NotNull JsonNumber asJsonNumber() {
		throw new ClassCastException("Not a JsonNumber!");
	}

	/**
	 * Casts this object to {@link JsonArray}.
	 *
	 * @return this object as a {@link JsonArray}.
	 *
	 * @throws ClassCastException If this object is not a {@link JsonArray}.
	 */
	default @NotNull JsonArray asJsonArray() {
		throw new ClassCastException("Not a JsonArray!");
	}

	/**
	 * Casts this object to {@link JsonObject}.
	 *
	 * @return this object as a {@link JsonObject}.
	 *
	 * @throws ClassCastException If this object is not a {@link JsonObject}.
	 */
	default @NotNull JsonObject asJsonObject() {
		throw new ClassCastException("Not a JsonObject!");
	}
}
