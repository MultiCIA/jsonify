/*
 * Copyright (C) 2022-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.jsonify;

import org.jetbrains.annotations.NotNull;

import java.io.Serial;
import java.util.ArrayList;
import java.util.Collection;

/**
 * JSON Array. While this {@link JsonArray} implementation allows {@code null} elements, their use is discouraged. The
 * {@link JsonWriter} will treat {@code null} as {@link JsonKeyword#NULL}, but {@link Object#equals(Object)} will not.
 */
public final class JsonArray extends ArrayList<JsonElement> implements JsonElement {
	@Serial private static final long serialVersionUID = 0L;


	public JsonArray() {
	}

	public JsonArray(@NotNull Collection<? extends @NotNull JsonElement> collection) {
		super(collection);
	}


	@Override
	public @NotNull JsonArray asJsonArray() {
		return this;
	}
}
