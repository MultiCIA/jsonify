/*
 * Copyright (C) 2022-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.jsonify;

import org.jetbrains.annotations.NotNull;

public sealed interface JsonImmutable extends JsonElement permits JsonKeyword, JsonNumber, JsonString {
	/**
	 * Casts this object to {@link JsonImmutable}.
	 *
	 * @return this object as a {@link JsonImmutable}.
	 *
	 * @throws ClassCastException If this object is not a {@link JsonImmutable}.
	 */
	@Override
	default @NotNull JsonImmutable asJsonImmutable() {
		return this;
	}
}
