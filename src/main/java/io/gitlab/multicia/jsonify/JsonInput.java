/*
 * Copyright (C) 2022-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.jsonify;

import org.jetbrains.annotations.NotNull;

import java.io.Closeable;
import java.io.IOException;

public interface JsonInput extends Closeable {
	/**
	 * Parses and returns the next token. Upon reaching the end of an array or object, it repeatedly returns
	 * {@link JsonTokens#STRUCTURE_END} until {@link #endStructure()} is called.
	 *
	 * @throws IOException if an error occurs while reading from or parsing the input.
	 */
	@NotNull JsonToken nextToken() throws IOException;

	/**
	 * Skip over the end of an array or an object. This method will also skip over any unread tokens before
	 * {@link JsonTokens#STRUCTURE_END}.
	 *
	 * @throws IllegalStateException if the parser is not currently inside a structure.
	 * @throws IOException if an error occurs while reading from or parsing the input.
	 */
	void endStructure() throws IOException;

	/**
	 * Parses a structure if the previous token is {@link JsonTokens#ARRAY_BEGIN} or {@link JsonTokens#OBJECT_BEGIN}.
	 * Returns the corresponding {@link JsonArray} or {@link JsonObject} and automatically skips
	 * {@link JsonTokens#STRUCTURE_END}.
	 *
	 * @throws IllegalStateException if the previous token is not the start of a structure.
	 * @throws IOException if an error occurs while reading from or parsing the input.
	 */
	@NotNull JsonElement parseStructure() throws IOException;
}
