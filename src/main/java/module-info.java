module io.gitlab.multicia.jsonify {
	requires static transitive org.jetbrains.annotations;

	exports io.gitlab.multicia.jsonify;
}
