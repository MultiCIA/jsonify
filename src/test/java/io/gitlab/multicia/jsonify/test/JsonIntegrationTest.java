/*
 * Copyright (C) 2022-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.jsonify.test;

import io.gitlab.multicia.jsonify.JsonArray;
import io.gitlab.multicia.jsonify.JsonElement;
import io.gitlab.multicia.jsonify.JsonReader;
import io.gitlab.multicia.jsonify.JsonWriter;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public class JsonIntegrationTest {
	private static @NotNull String loadJsonFromResource() {
		try (
				final InputStream inputStream = JsonIntegrationTest.class.getResourceAsStream("large-file.json");
				final Reader reader = new InputStreamReader(
						Objects.requireNonNull(inputStream, "Failed loading test resource!"), StandardCharsets.UTF_8)
		) {
			final StringWriter writer = new StringWriter();
			reader.transferTo(writer);
			return writer.toString();
		} catch (final IOException exception) {
			throw new AssertionError("Failed loading test resource!", exception);
		}
	}
	
	private final static @NotNull String JSON_FROM_RESOURCE = loadJsonFromResource();

	@Test
	public void bigInput() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonArray array = JsonReader.read(JSON_FROM_RESOURCE).asJsonArray();
			Assertions.assertEquals(11351, array.size());
			final String writtenJson = JsonWriter.write(array);
			final JsonElement readArray = JsonReader.read(writtenJson).asJsonArray();
			Assertions.assertEquals(array, readArray);
		});
	}
}
