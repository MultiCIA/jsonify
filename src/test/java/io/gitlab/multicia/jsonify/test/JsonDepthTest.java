/*
 * Copyright (C) 2022-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.jsonify.test;

import io.gitlab.multicia.jsonify.JsonArray;
import io.gitlab.multicia.jsonify.JsonElement;
import io.gitlab.multicia.jsonify.JsonObject;
import io.gitlab.multicia.jsonify.JsonReader;
import io.gitlab.multicia.jsonify.JsonWriter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class JsonDepthTest {
	@ParameterizedTest
	@ValueSource(ints = {10, 1000, 100000})
	public void testReaderDeepArray(int depth) {
		Assertions.assertDoesNotThrow(() -> {
			final StringBuilder builder = new StringBuilder();
			builder.repeat("[", depth).repeat("]", depth);
			JsonElement element = JsonReader.read(builder.toString());
			for (int i = 1; i < depth; i++) {
				element = element.asJsonArray().getFirst();
			}
			Assertions.assertTrue(element.asJsonArray().isEmpty());
		});
	}

	@ParameterizedTest
	@ValueSource(ints = {10, 1000, 100000})
	public void testReaderDeepMap(int depth) {
		Assertions.assertDoesNotThrow(() -> {
			final StringBuilder builder = new StringBuilder();
			builder.repeat("{\"\":", depth - 1).append("{}").repeat("}", depth - 1);
			JsonElement element = JsonReader.read(builder.toString());
			for (int i = 1; i < depth; i++) {
				element = element.asJsonObject().get("");
			}
			Assertions.assertTrue(element.asJsonObject().isEmpty());
		});
	}

	@ParameterizedTest
	@ValueSource(ints = {10, 1000, 100000})
	public void testReaderDeepObjectArray(int depth) {
		Assertions.assertDoesNotThrow(() -> {
			final StringBuilder builder = new StringBuilder();
			builder.repeat("{\"\":[", depth).repeat("]}", depth);
			JsonElement element = JsonReader.read(builder.toString());
			for (int i = 1; i < depth; i++) {
				element = element.asJsonObject().get("");
				element = element.asJsonArray().getFirst();
			}
			Assertions.assertTrue(element.asJsonObject().get("").asJsonArray().isEmpty());
		});
	}

	@ParameterizedTest
	@ValueSource(ints = {10, 1000, 100000})
	public void testWriterDeepArray(int depth) {
		Assertions.assertDoesNotThrow(() -> {
			final JsonArray array = new JsonArray();
			JsonArray currentArray = array;
			for (int i = 1; i < depth; i++) {
				final JsonArray innerArray = new JsonArray();
				currentArray.add(innerArray);
				currentArray = innerArray;
			}
			Assertions.assertEquals(
					JsonWriter.write(array),
					new StringBuilder().repeat("[", depth).repeat("]", depth).toString()
			);
		});
	}

	@ParameterizedTest
	@ValueSource(ints = {10, 1000, 100000})
	public void testWriterDeepObject(int depth) {
		Assertions.assertDoesNotThrow(() -> {
			final JsonObject object = new JsonObject();
			JsonObject currentObject = object;
			for (int i = 1; i < depth; i++) {
				final JsonObject innerObject = new JsonObject();
				currentObject.put("", innerObject);
				currentObject = innerObject;
			}
			Assertions.assertEquals(
					JsonWriter.write(object),
					new StringBuilder().repeat("{\"\":", depth - 1).append("{}").repeat("}", depth - 1).toString()
			);
		});
	}

	@ParameterizedTest
	@ValueSource(ints = {10, 1000, 100000})
	public void testWriterDeepObjectArray(int depth) {
		Assertions.assertDoesNotThrow(() -> {
			final JsonObject object = new JsonObject();
			JsonObject currentObject = object;
			for (int i = 1; i < depth; i++) {
				final JsonArray innerArray = new JsonArray();
				final JsonObject innerObject = new JsonObject();
				innerArray.add(innerObject);
				currentObject.put("", innerArray);
				currentObject = innerObject;
			}
			currentObject.put("", new JsonArray());
			Assertions.assertEquals(
					JsonWriter.write(object),
					new StringBuilder().repeat("{\"\":[", depth).repeat("]}", depth).toString()
			);
		});
	}
}
