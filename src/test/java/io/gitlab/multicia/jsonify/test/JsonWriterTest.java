/*
 * Copyright (C) 2022-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.jsonify.test;

import io.gitlab.multicia.jsonify.JsonArray;
import io.gitlab.multicia.jsonify.JsonElement;
import io.gitlab.multicia.jsonify.JsonKeyword;
import io.gitlab.multicia.jsonify.JsonNumber;
import io.gitlab.multicia.jsonify.JsonObject;
import io.gitlab.multicia.jsonify.JsonString;
import io.gitlab.multicia.jsonify.JsonWriter;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.Writer;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.SequencedMap;

/**
 * Test the {@link JsonWriter} functionality.
 */
public class JsonWriterTest {
	@Test
	public void writeEmpty() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonWriter writer = new JsonWriter(Writer.nullWriter());
			writer.close();
			Assertions.assertFalse(writer.isDone());
		});
	}

	@Test
	public void writeAfterDocumentEnd() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonWriter writer = new JsonWriter(Writer.nullWriter());
			writer.value(JsonKeyword.NULL);
			Assertions.assertThrows(IllegalStateException.class, () -> writer.value(JsonKeyword.NULL));
			writer.close();
		});
	}

	@Test
	public void writeUnexpectedStructureEnd() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonWriter writer = new JsonWriter(Writer.nullWriter());
			Assertions.assertThrows(IllegalStateException.class, writer::end);
			writer.close();
		});
	}

	@Test
	public void writeUnexpectedName() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonWriter writer = new JsonWriter(Writer.nullWriter());
			writer.beginArray();
			Assertions.assertThrows(IllegalStateException.class, () -> writer.name(""));
			writer.close();
		});
	}

	@Test
	public void writeUnexpectedValue() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonWriter writer = new JsonWriter(Writer.nullWriter());
			writer.beginObject();
			Assertions.assertThrows(IllegalStateException.class, () -> writer.value(JsonKeyword.NULL));
			writer.close();
		});
	}

	@Test
	public void writeUnexpectedBeginObject() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonWriter writer = new JsonWriter(Writer.nullWriter());
			writer.beginObject();
			Assertions.assertThrows(IllegalStateException.class, writer::beginObject);
			writer.close();
		});
	}

	@Test
	public void writeUnexpectedBeginArray() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonWriter writer = new JsonWriter(Writer.nullWriter());
			writer.beginObject();
			Assertions.assertThrows(IllegalStateException.class, writer::beginArray);
			writer.close();
		});
	}

	// ====================

	@Test
	public void saveArrayContainsNothing() {
		Assertions.assertDoesNotThrow(() -> Assertions.assertEquals(
				"[]",
				JsonWriter.write(new JsonArray())
		));
	}

	@Test
	public void saveArrayContainsKeywords() {
		Assertions.assertDoesNotThrow(() -> Assertions.assertEquals(
				"[null,false,true]",
				JsonWriter.write(new JsonArray(List.of(JsonKeyword.NULL, JsonKeyword.FALSE, JsonKeyword.TRUE)))
		));
	}

	@Test
	public void saveArrayContainsStringAndNumber() {
		Assertions.assertDoesNotThrow(() -> Assertions.assertEquals(
				"[\"\",0]",
				JsonWriter.write(new JsonArray(List.of(new JsonString(""), new JsonNumber(0))))
		));
	}

	@Test
	public void saveArrayContainsArrayAndObject() {
		Assertions.assertDoesNotThrow(() -> Assertions.assertEquals(
				"[[],{}]",
				JsonWriter.write(new JsonArray(List.of(new JsonArray(), new JsonObject())))
		));
	}

	@Test
	public void saveArrayContainsArrayContainsArray() {
		Assertions.assertDoesNotThrow(() -> Assertions.assertEquals(
				"[[[]]]",
				JsonWriter.write(new JsonArray(List.of(new JsonArray(List.of(new JsonArray())))))
		));
	}

	// ====================

	@SuppressWarnings("SameParameterValue")
	private static @NotNull SequencedMap<@NotNull String, @NotNull JsonElement> mapOf(
			@NotNull String keyA, @NotNull JsonElement valueA
	) {
		final SequencedMap<String, JsonElement> map = new LinkedHashMap<>();
		map.put(keyA, valueA);
		return map;
	}

	@SuppressWarnings("SameParameterValue")
	private static @NotNull SequencedMap<@NotNull String, @NotNull JsonElement> mapOf(
			@NotNull String keyA, @NotNull JsonElement valueA,
			@NotNull String keyB, @NotNull JsonElement valueB
	) {
		final SequencedMap<String, JsonElement> map = new LinkedHashMap<>();
		map.put(keyA, valueA);
		map.put(keyB, valueB);
		return map;
	}

	@SuppressWarnings("SameParameterValue")
	private static @NotNull SequencedMap<@NotNull String, @NotNull JsonElement> mapOf(
			@NotNull String keyA, @NotNull JsonElement valueA,
			@NotNull String keyB, @NotNull JsonElement valueB,
			@NotNull String keyC, @NotNull JsonElement valueC
	) {
		final SequencedMap<String, JsonElement> map = new LinkedHashMap<>();
		map.put(keyA, valueA);
		map.put(keyB, valueB);
		map.put(keyC, valueC);
		return map;
	}

	@Test
	public void saveObjectContainsNothing() {
		Assertions.assertDoesNotThrow(() -> Assertions.assertEquals(
				"{}",
				JsonWriter.write(new JsonObject())
		));
	}

	@Test
	public void saveObjectContainsKeywords() {
		Assertions.assertDoesNotThrow(() -> Assertions.assertEquals(
				"{\"1\":null,\"2\":false,\"3\":true}",
				JsonWriter.write(new JsonObject(mapOf(
						"1", JsonKeyword.NULL,
						"2", JsonKeyword.FALSE,
						"3", JsonKeyword.TRUE
				)))
		));
	}

	@Test
	public void saveObjectContainsStringAndNumber() {
		Assertions.assertDoesNotThrow(() -> Assertions.assertEquals(
				"{\"1\":\"\",\"2\":0}",
				JsonWriter.write(new JsonObject(mapOf(
						"1", new JsonString(""),
						"2", new JsonNumber(0)
				)))
		));
	}

	@Test
	public void saveObjectContainsArrayAndObject() {
		Assertions.assertDoesNotThrow(() -> Assertions.assertEquals(
				"{\"1\":[],\"2\":{}}",
				JsonWriter.write(new JsonObject(mapOf(
						"1", new JsonArray(),
						"2", new JsonObject()
				)))
		));
	}

	@Test
	public void saveObjectContainsObjectContainsObject() {
		Assertions.assertDoesNotThrow(() -> Assertions.assertEquals(
				"{\"\":{\"\":{}}}",
				JsonWriter.write(new JsonObject(mapOf(
						"", new JsonObject(mapOf(
								"", new JsonObject()
						))
				)))
		));
	}

	// ====================

	@Test
	public void saveArrayRecursive() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			final JsonArray arrayA = new JsonArray();
			final JsonArray arrayB = new JsonArray();
			arrayA.add(arrayB);
			arrayB.add(arrayA);
			JsonWriter.write(Writer.nullWriter(), arrayA);
		});
	}

	@Test
	public void saveObjectRecursive() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			final JsonObject objectA = new JsonObject();
			final JsonObject objectB = new JsonObject();
			objectA.put("", objectB);
			objectB.put("", objectA);
			JsonWriter.write(Writer.nullWriter(), objectA);
		});
	}

	@Test
	public void saveArrayObjectRecursive() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			final JsonArray array = new JsonArray();
			final JsonObject object = new JsonObject();
			array.add(object);
			object.put("", array);
			JsonWriter.write(Writer.nullWriter(), object);
		});
	}

	// ====================

	@Test
	public void saveKeywords() {
		Assertions.assertDoesNotThrow(() -> {
			Assertions.assertEquals("null", JsonWriter.write(JsonKeyword.NULL));
			Assertions.assertEquals("true", JsonWriter.write(JsonKeyword.TRUE));
			Assertions.assertEquals("false", JsonWriter.write(JsonKeyword.FALSE));
		});
	}

	@Test
	public void saveStrings() {
		Assertions.assertDoesNotThrow(() -> {
			Assertions.assertEquals("\"\"", JsonWriter.write(new JsonString("")));
			Assertions.assertEquals("\"\\n\\r\\b\\t\\f\\\\ABCDEFGHIJKLMNOPQRSTUVWXYZ<>:{}" +
							"abcdefghijklmnopqrstuvwxyz,.;'[]/`123456789-=~!@#$%^&*_+()К௪ၐᎺអὲ⍚❂⼒ぐ㋺ꁐꁚꑂ中\"",
					JsonWriter.write(new JsonString("\n\r\b\t\f\\ABCDEFGHIJKLMNOPQRSTUVWXYZ<>:{}" +
							"abcdefghijklmnopqrstuvwxyz,.;'[]/`123456789-=~!@#$%^&*_+()К௪ၐᎺអὲ⍚❂⼒ぐ㋺ꁐꁚꑂ中")));
		});
	}

	// ====================

	@Test
	public void saveNumbers() {
		Assertions.assertDoesNotThrow(() -> {
			// integer small
			Assertions.assertEquals(
					String.valueOf(Integer.MAX_VALUE),
					JsonWriter.write(new JsonNumber(Integer.MAX_VALUE))
			);
			// integer medium
			Assertions.assertEquals(
					String.valueOf(Long.MAX_VALUE),
					JsonWriter.write(new JsonNumber(Long.MAX_VALUE))
			);
			// integer big
			Assertions.assertEquals(
					"123456789012345678901234567890",
					JsonWriter.write(new JsonNumber(new BigInteger("123456789012345678901234567890")))
			);
			// decimal fraction small
			Assertions.assertEquals(
					"0.12345678",
					JsonWriter.write(new JsonNumber(0.12345678))
			);
			// decimal fraction medium
			Assertions.assertEquals(
					"0.12345678912345678",
					JsonWriter.write(new JsonNumber(0.12345678912345678))
			);
			// decimal fraction big
			Assertions.assertEquals(
					"0.12345678901234567890",
					JsonWriter.write(new JsonNumber(new BigDecimal("0.12345678901234567890")))
			);
			// decimal fraction trailing zero
			Assertions.assertEquals(
					"0.1",
					JsonWriter.write(new JsonNumber(new BigDecimal("0.1000")))
			);
			// decimal fraction exponent
			Assertions.assertEquals(
					"1.0E9",
					JsonWriter.write(new JsonNumber(new BigDecimal("1.0e9")))
			);
			// decimal fraction trailing zero exponent
			Assertions.assertEquals(
					"1.0E9",
					JsonWriter.write(new JsonNumber(new BigDecimal("1.0000e9")))
			);
			// decimal exponent
			Assertions.assertEquals(
					"1.0E9",
					JsonWriter.write(new JsonNumber(new BigDecimal("1e9")))
			);
		});
	}
}
