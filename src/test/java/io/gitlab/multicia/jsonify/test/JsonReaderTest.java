/*
 * Copyright (C) 2022-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.jsonify.test;

import io.gitlab.multicia.jsonify.JsonArray;
import io.gitlab.multicia.jsonify.JsonElement;
import io.gitlab.multicia.jsonify.JsonKeyword;
import io.gitlab.multicia.jsonify.JsonNumber;
import io.gitlab.multicia.jsonify.JsonObject;
import io.gitlab.multicia.jsonify.JsonReader;
import io.gitlab.multicia.jsonify.JsonString;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

/**
 * Test the {@link JsonReader#read(Reader)} functionality.
 */
public class JsonReaderTest {
	private static final String WHITESPACES = " \n\r\t\r\n";

	@Test
	public void loadArrayEmpty() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("[]"));
			Assertions.assertEquals(JsonArray.class, element.getClass());
			Assertions.assertEquals(List.of(), element);
		});
	}

	@Test
	public void loadArrayNull() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("[null]"));
			Assertions.assertEquals(JsonArray.class, element.getClass());
			Assertions.assertEquals(List.of(JsonKeyword.NULL), element);
		});
	}

	@Test
	public void loadArrayFalse() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("[false]"));
			Assertions.assertEquals(JsonArray.class, element.getClass());
			Assertions.assertEquals(List.of(JsonKeyword.FALSE), element);
		});
	}

	@Test
	public void loadArrayTrue() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("[true]"));
			Assertions.assertEquals(JsonArray.class, element.getClass());
			Assertions.assertEquals(List.of(JsonKeyword.TRUE), element);
		});
	}

	@Test
	public void loadArrayString() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("[\"\"]"));
			Assertions.assertEquals(JsonArray.class, element.getClass());
			Assertions.assertEquals(List.of(new JsonString("")), element);
		});
	}

	@Test
	public void loadArrayNumber() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("[0]"));
			Assertions.assertEquals(JsonArray.class, element.getClass());
			Assertions.assertEquals(List.of(new JsonNumber(0)), element);
		});
	}

	@Test
	public void loadArrayArray() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("[[]]"));
			Assertions.assertEquals(JsonArray.class, element.getClass());
			Assertions.assertEquals(List.of(List.of()), element);
		});
	}

	@Test
	public void loadArrayObject() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("[{}]"));
			Assertions.assertEquals(JsonArray.class, element.getClass());
			Assertions.assertEquals(List.of(Map.of()), element);
		});
	}

	@Test
	public void loadArrayDouble() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("[null,null]"));
			Assertions.assertEquals(JsonArray.class, element.getClass());
			Assertions.assertEquals(List.of(JsonKeyword.NULL, JsonKeyword.NULL), element);
		});
	}

	@Test
	public void loadArrayTriple() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("[null,null,null]"));
			Assertions.assertEquals(JsonArray.class, element.getClass());
			Assertions.assertEquals(List.of(JsonKeyword.NULL, JsonKeyword.NULL, JsonKeyword.NULL), element);
		});
	}

	@Test
	public void loadArrayWhitespace() {
		Assertions.assertDoesNotThrow(() -> {
			final String input = String.join(WHITESPACES, "", "[", "null", ",", "null", ",", "null", "]", "");
			final JsonElement element = JsonReader.read(new StringReader(input));
			Assertions.assertEquals(JsonArray.class, element.getClass());
			Assertions.assertEquals(List.of(JsonKeyword.NULL, JsonKeyword.NULL, JsonKeyword.NULL), element);
		});
	}

	// ==========

	@Test
	public void throwArrayNoClose() {
		Assertions.assertThrows(IOException.class, () -> JsonReader.read(new StringReader("[")));
	}

	@Test
	public void throwArrayNoOpen() {
		Assertions.assertThrows(IOException.class, () -> JsonReader.read(new StringReader("]")));
	}

	@Test
	public void throwArrayExtraAfter() {
		Assertions.assertThrows(IOException.class, () -> JsonReader.read(new StringReader("[],")));
	}

	@Test
	public void throwArrayExtraEmptyInside() {
		Assertions.assertThrows(IOException.class, () -> JsonReader.read(new StringReader("[,]")));
	}

	@Test
	public void throwArrayExtraSingleInside() {
		Assertions.assertThrows(IOException.class, () -> JsonReader.read(new StringReader("[null,]")));
	}

	// ====================

	@Test
	public void loadObjectEmpty() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("{}"));
			Assertions.assertEquals(JsonObject.class, element.getClass());
			Assertions.assertEquals(Map.of(), element);
		});
	}

	@Test
	public void loadObjectNull() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("{\"\":null}"));
			Assertions.assertEquals(JsonObject.class, element.getClass());
			Assertions.assertEquals(Map.of("", JsonKeyword.NULL), element);
		});
	}

	@Test
	public void loadObjectFalse() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("{\"\":false}"));
			Assertions.assertEquals(JsonObject.class, element.getClass());
			Assertions.assertEquals(Map.of("", JsonKeyword.FALSE), element);
		});
	}

	@Test
	public void loadObjectTrue() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("{\"\":true}"));
			Assertions.assertEquals(JsonObject.class, element.getClass());
			Assertions.assertEquals(Map.of("", JsonKeyword.TRUE), element);
		});
	}

	@Test
	public void loadObjectString() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("{\"\":\"\"}"));
			Assertions.assertEquals(JsonObject.class, element.getClass());
			Assertions.assertEquals(Map.of("", new JsonString("")), element);
		});
	}

	@Test
	public void loadObjectNumber() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("{\"\":0}"));
			Assertions.assertEquals(JsonObject.class, element.getClass());
			Assertions.assertEquals(Map.of("", new JsonNumber(0)), element);
		});
	}

	@Test
	public void loadObjectArray() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("{\"\":[]}"));
			Assertions.assertEquals(JsonObject.class, element.getClass());
			Assertions.assertEquals(Map.of("", List.of()), element);
		});
	}

	@Test
	public void loadObjectObject() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("{\"\":{}}"));
			Assertions.assertEquals(JsonObject.class, element.getClass());
			Assertions.assertEquals(Map.of("", Map.of()), element);
		});
	}

	@Test
	public void loadObjectDouble() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("{\"\":null,\"2\":null}"));
			Assertions.assertEquals(JsonObject.class, element.getClass());
			Assertions.assertEquals(Map.of("", JsonKeyword.NULL, "2", JsonKeyword.NULL), element);
		});
	}

	@Test
	public void loadObjectTriple() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("{\"\":null,\"2\":null,\"3\":null}"));
			Assertions.assertEquals(JsonObject.class, element.getClass());
			Assertions.assertEquals(Map.of(
					"", JsonKeyword.NULL,
					"2", JsonKeyword.NULL,
					"3", JsonKeyword.NULL
			), element);
		});
	}

	@Test
	public void loadObjectWhitespace() {
		Assertions.assertDoesNotThrow(() -> {
			final String input = String.join(WHITESPACES,
					"", "{", "\"\"", ":", "null", ",", "\"2\"", ":", "null", ",", "\"3\"", ":", "null", "}", "");
			final JsonElement element = JsonReader.read(new StringReader(input));
			Assertions.assertEquals(JsonObject.class, element.getClass());
			Assertions.assertEquals(Map.of(
					"", JsonKeyword.NULL,
					"2", JsonKeyword.NULL,
					"3", JsonKeyword.NULL
			), element);
		});
	}

	@Test
	public void loadObjectDuplicates() {
		Assertions.assertDoesNotThrow(() -> {
			final String input = String.join(WHITESPACES,
					"", "{", "\"\"", ":", "null", ",", "\"\"", ":", "true", ",", "\"\"", ":", "false", "}", "");
			final JsonElement element = JsonReader.read(new StringReader(input));
			Assertions.assertEquals(JsonObject.class, element.getClass());
			Assertions.assertEquals(Map.of("", JsonKeyword.FALSE), element);
		});
	}

	// ====================

	@Test
	public void throwObjectNoClose() {
		Assertions.assertThrows(IOException.class, () -> JsonReader.read(new StringReader("{")));
	}

	@Test
	public void throwObjectNoOpen() {
		Assertions.assertThrows(IOException.class, () -> JsonReader.read(new StringReader("}")));
	}

	@Test
	public void throwObjectExtraAfter() {
		Assertions.assertThrows(IOException.class, () -> JsonReader.read(new StringReader("{},")));
	}

	@Test
	public void throwObjectExtraInsideEmpty() {
		Assertions.assertThrows(IOException.class, () -> JsonReader.read(new StringReader("{,}")));
	}

	@Test
	public void throwObjectExtraInsideSingle() {
		Assertions.assertThrows(IOException.class, () -> JsonReader.read(new StringReader("{\"\":null,}")));
	}

	@Test
	public void throwObjectIncompleteNoColon() {
		Assertions.assertThrows(IOException.class, () -> JsonReader.read(new StringReader("{\"\"")));
	}

	@Test
	public void throwObjectIncompleteNoValue() {
		Assertions.assertThrows(IOException.class, () -> JsonReader.read(new StringReader("{\"\":")));
	}

	@Test
	public void throwObjectIncompleteNoComma() {
		Assertions.assertThrows(IOException.class, () -> JsonReader.read(new StringReader("{\"\":null")));
	}

	@Test
	public void throwObjectIncompleteNoClose() {
		Assertions.assertThrows(IOException.class, () -> JsonReader.read(new StringReader("{\"\":null,")));
	}

	// ====================

	@Test
	public void loadNull() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("null"));
			Assertions.assertEquals(JsonKeyword.NULL, element);
		});
	}

	@Test
	public void loadTrue() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("true"));
			Assertions.assertEquals(JsonKeyword.TRUE, element);
		});
	}

	@Test
	public void loadFalse() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("false"));
			Assertions.assertEquals(JsonKeyword.FALSE, element);
		});
	}

	// ====================

	@Test
	public void throwNullExtra() {
		Assertions.assertThrows(IOException.class, () -> JsonReader.read(new StringReader("null,")));
	}

	@Test
	public void throwTrueExtra() {
		Assertions.assertThrows(IOException.class, () -> JsonReader.read(new StringReader("true,")));
	}

	@Test
	public void throwFalseExtra() {
		Assertions.assertThrows(IOException.class, () -> JsonReader.read(new StringReader("false,")));
	}

	// ====================

	@Test
	public void loadStringEmpty() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("\"\""));
			Assertions.assertEquals(new JsonString(""), element);
		});
	}

	@Test
	public void loadStringNormal() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader(
					"\"ABCDEFGHIJKLMNOPQRSTUVWXYZ<>:{}abcdefghijklmnopqrstuvwxyz,.;'[]\\/`123456789-=~!@#$%^&*_+()" +
							"\\r\\b\\n\\t\\f\\\\К௪ၐᎺអὲ⍚❂⼒ぐ㋺ꁐꁚꑂ\\u4e2d\""));
			Assertions.assertEquals(new JsonString(
					"ABCDEFGHIJKLMNOPQRSTUVWXYZ<>:{}abcdefghijklmnopqrstuvwxyz,.;'[]/`123456789-=~!@#$%^&*_+()" +
							"\r\b\n\t\f\\К௪ၐᎺអὲ⍚❂⼒ぐ㋺ꁐꁚꑂ中"), element);
		});
	}

	// ====================

	@Test
	public void throwStringExtra() {
		Assertions.assertThrows(IOException.class, () -> JsonReader.read(new StringReader("\"\",")));
	}

	// ====================

	@Test
	public void loadNumberIntegerSmall() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("0"));
			Assertions.assertEquals(new JsonNumber(0), element);
		});
	}

	@Test
	public void loadNumberIntegerBig() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("123456789012345678901234567890"));
			Assertions.assertEquals(new JsonNumber(new BigInteger("123456789012345678901234567890")), element);
		});
	}

	@Test
	public void loadNumberIntegerNegative() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("-123456789012345678901234567890"));
			Assertions.assertEquals(new JsonNumber(new BigInteger("-123456789012345678901234567890")), element);
		});
	}

	@Test
	public void loadNumberDecimalFractionSmall() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("0.12345678"));
			Assertions.assertEquals(new JsonNumber(0.12345678), element);
		});
	}

	@Test
	public void loadNumberDecimalFractionMedium() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("0.12345678912345678"));
			Assertions.assertEquals(new JsonNumber(0.12345678912345678), element);
		});
	}

	@Test
	public void loadNumberDecimalFractionBig() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("0.123456789012345678901234567890"));
			Assertions.assertEquals(new JsonNumber(new BigDecimal("0.123456789012345678901234567890")), element);
		});
	}

	@Test
	public void loadNumberDecimalFractionNegative() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("-0.123456789012345678901234567890"));
			Assertions.assertEquals(new JsonNumber(new BigDecimal("-0.123456789012345678901234567890")), element);
		});
	}

	@Test
	public void loadNumberDecimalFractionTrailingZero() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("0.1000"));
			Assertions.assertEquals(new JsonNumber(new BigDecimal("0.1000")), element);
		});
	}

	@Test
	public void loadNumberDecimalFractionExponent() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("1.0e9"));
			Assertions.assertEquals(new JsonNumber(new BigDecimal("1.0E+9")), element);
		});
	}

	@Test
	public void loadNumberDecimalFractionTrailingZeroExponent() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("1.0000e9"));
			Assertions.assertEquals(new JsonNumber(new BigDecimal("1.0000E+9")), element);
		});
	}

	@Test
	public void loadNumberDecimalExponent() {
		Assertions.assertDoesNotThrow(() -> {
			final JsonElement element = JsonReader.read(new StringReader("1e9"));
			Assertions.assertEquals(new JsonNumber(new BigDecimal("1E+9")), element);
		});
	}

	// ====================

	@Test
	public void throwNumberExtra() {
		Assertions.assertThrows(IOException.class, () -> JsonReader.read(new StringReader("0,")));
	}
}
