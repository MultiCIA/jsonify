/*
 * Copyright (C) 2022-2024 Mai Thanh Minh (a.k.a. thanhminhmr)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package io.gitlab.multicia.jsonify.test;

import io.gitlab.multicia.jsonify.JsonArray;
import io.gitlab.multicia.jsonify.JsonElement;
import io.gitlab.multicia.jsonify.JsonKeyword;
import io.gitlab.multicia.jsonify.JsonObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JsonConverterTest {
	@Test
	public void testNotRecursive() {
		Assertions.assertDoesNotThrow(() -> {
			Assertions.assertEquals(new JsonArray(), JsonElement.of(List.of()));
			Assertions.assertEquals(new JsonObject(), JsonElement.of(Map.of()));
			Assertions.assertEquals(new JsonArray(List.of(new JsonObject())), JsonElement.of(List.of(Map.of())));
			Assertions.assertEquals(new JsonObject(Map.of("", new JsonArray())), JsonElement.of(Map.of("", List.of())));
		});
	}

	@Test
	public void testRepeatedNotRecursive() {
		Assertions.assertDoesNotThrow(() -> {
			final List<Object> list = new ArrayList<>();
			final Map<String, Object> map = new HashMap<>();
			list.add(map);
			list.add(map);
			map.put("", Boolean.TRUE);
			Assertions.assertEquals(new JsonArray(List.of(
					new JsonObject(Map.of("", JsonKeyword.TRUE)),
					new JsonObject(Map.of("", JsonKeyword.TRUE))
			)), JsonElement.of(list));
		});
	}

	@Test
	public void testRecursive() {
		final List<Object> list = new ArrayList<>();
		final Map<String, Object> map = new HashMap<>();
		list.add(map);
		map.put("", list);
		Assertions.assertThrows(IllegalArgumentException.class, () -> JsonElement.of(list));
		Assertions.assertThrows(IllegalArgumentException.class, () -> JsonElement.of(map));
	}
}
