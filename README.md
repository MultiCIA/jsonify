# jsonify

<img src="/misc/jsonify.png" width="128px">

A production-ready JSON reader/writer library with a stable API. Future changes will follow deprecation policies, with
at least one year of notice before any API removal.

## Goals

- Fully compliant with JSON standards.
- Easy to maintain and extend.
- Simple and intuitive API.
- Optimized for high performance.

## Non-goals

- Not intended as a serializer/deserializer. For those, consider libraries
  like [Jackson](https://github.com/FasterXML/jackson) or [Gson](https://github.com/google/gson).
- Does not provide detailed location info on parsing errors. For that, use a JSON validator or alternative libraries for
  non-standard JSON.

## Usage

### High-level Usage

To read and parse JSON:

```java
// Read a JSON array from a java.io.Reader
JsonArray array = JsonReader.read(reader).asJsonArray();

// Read a JSON object from a java.lang.String
JsonObject object = JsonReader.read(jsonString).asJsonObject();
```

To write a `JsonElement` back to JSON:

```java
// Write an element to a java.io.Writer
JsonWriter.write(writer, element);

// Write an element to a java.lang.String
String jsonString = JsonWriter.write(element);
```

> **Note**: `JsonWriter::write` checks for circular references and will throw a `JsonRuntimeException` if detected.

#### JSON Element Types

- `JsonArray` for arrays
- `JsonObject` for objects
- `JsonString` for strings
- `JsonNumber` for numbers
- `JsonKeyword` for booleans and `null`

`JsonArray` and `JsonObject` are mutable. `JsonString`, `JsonNumber`, and `JsonKeyword` are immutable and subclasses of
`JsonImmutable`.

### Low-level Usage

For more granular control, use instances of `JsonReader` and `JsonWriter` directly. This allows skipping portions of
JSON during reading to save time and memory.

## To-Do

- [x] Low-level JSON reading.
- [x] Low-level JSON writing.
- [x] High-level JSON parsing.
- [x] High-level JSON writing.
- [ ] Benchmark against other libraries.

## License

LGPL v3 or later.

## Found a Bug?

Feel free to open an issue.